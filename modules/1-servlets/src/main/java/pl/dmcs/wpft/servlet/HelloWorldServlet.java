package pl.dmcs.wpft.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Robert Ritter
 */
@WebServlet(name = "HelloWorldServlet", urlPatterns = {"/hello"})
public class HelloWorldServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(HelloWorldServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.info("GET {} HelloWorldServlet", request.getRemoteAddr());
//        response.getWriter().append("Hello!").close();
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/test1.jsp");
        if (requestDispatcher != null) {
            requestDispatcher.include(request, response);
        }
    }
}
