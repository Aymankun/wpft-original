package pl.dmcs.wpft.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author Robert Ritter
 */
public class Hello2Filter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        //request to servlet
        chain.doFilter(req, resp);

        // response from servlet
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
